# TAREFAS A REALIZAR

#### **Erro de import de CSS** <br>
O website possui em seu escopo 3 folhas de estilos que devem ser importadas. são elas:
- style.css (pacote icomoon)
- home.css
- page404.css

<br>

    > Realizar a importação das folhas de estilos listadas acimas, respeitando a ordem da listagem.
    > O arquivo page404.css deve ser importado somente em caso de página de erro 404 do wordpress.

#### **Corrigir erro de largura máxima de Container no CSS** <br>
O portal deve possuir uma largura máxima de 1440px para o container do website. Existe um arquivo css de variáveis responsável por controlar esse valor.

<br>

    > Definir o valor de 1440px para a variável [width_container].

#### **Erro de fechamento de tag PHP** <br>
O arquivo footer é responsável por exibir informações como Endereço de sedes, Telefones de contato, Sitemap, Socialmedia, e Copyrights. Um erro de fechamento de tag PHP está impossibilitando que a sessão footer seja exibida corretamente no portal.

<br>

    > Encontrar a tag PHP que não está fechada corretamente e corrigir. Ao corrigir essa informação, todo o rodapé será exibido corretamente.

#### **Erro de include PHP** <br>
O arquivo footer é responsável por exibir informações como Endereço de sedes, Telefones de contato, Sitemap, Socialmedia, e Copyrights. Dentro da section Copyrights é preciso incluir um arquivo externo que possui as informações de copyrights da empresa.

<br>

    > Realizar Include com Locate Template do arquivo copyrights.php localizado dentro do diretório [template-parts]

#### **Corrigir erro de Custom Field Name incorreto** <br>
O plugin Advanced Custom Fields PRÓ é responsável por criar campos personalizados divididos em Grupos de Campos. Cada grupo de campo pode ser associado a um Custom Post Type.
Na sessão "Sala de Imprensa" existe um campo com chamada incorreta de acordo com o que foi definido em seu grupo de campos, localizado no dashboard acessível pelo menu Campos Personalizados.

- Ver documentação do Plugin ACF: [https://www.advancedcustomfields.com/resources/](https://www.advancedcustomfields.com/resources/)

<br>

    > Encontrar o campo que está com nome incorreto, e corrigir no arquivo pagetemplate-home.php com a nomenclatura correta definida no respectivo grupo de campos.

#### **Corrigir erro de custom field name (subfield)** <br>
O plugin Advanced Custom Fields PRÓ é responsável por criar campos personalizados divididos em Grupos de Campos. Cada grupo de campo pode ser associado a um Custom Post Type.
Na sessão de socialmedia no arquivo footer.php as redes sociais não estão sendo exibidas corretamente devido a uma chamada incorreta de campos de segundo nível de um tipo de campo repetidor.

<br>

    > Corrigir chamada de custom fields de segundo nível. Fields e Subfields do plugin ACF PRÓ.

#### **Corrigir erro de Custom Post Type Inexistente** <br>
O Wordpress permite criar novos Post Types a partir do arquivo functions.php. Cada novo Custom Post Type recebe um identificador definido como "register_post_type".
Na sessão de socialmedia no arquivo header.php as redes sociais não estão sendo exibidas corretamente devido a uma chamada incorreta do nome do custom post type.

<br>

    > Identificar o nome correto do custom post type de social media e corrigir no arquivo header.php no respectivo loop da sessão social media.

#### **CORREÇÃO DE ALINHAMENTO DAS REDES SOCIAIS FOOTER** <br>
Na sessão de socialmedia no arquivo footer.php as redes sociais não estão sendo alinhadas corretamente de acordo com o definido no layout figma. As redes sociais estão centralizadas, e devem ficar alinhadas a esquerda utilizando CSS Flexbox.

<br>

    > Corrigir alinhamento das redes sociais do footer.

#### **Criar nova estrutura de Custom Post Type & Custom Fields para Telefones** <br>
No arquivo footer.php os Telefones estão inseridos de forma estática. Dessa maneira, qualquer alteração desses dados deve ser realizado diretamente no código, e isso não é a forma correta de proceder. A forma correta é que no Dashboard do Wordpress haja um local para que seja possível cadastrar essas informações, e elas sejam exibidas no footer.

<br>

    > Criar Custom Post Type
        - nome: cptphones
    > Criar Grupo de Campos e associar ao Custom Post Type criado anteriormente. O grupo de campos deve conter os seguintes campos:
        - DDI
        - DDD
        - Número
    > Inserir um novo registro no Custom Post Type criado.
    > Alterar no arquivo footer.php a sessão telefones, removendo os dados estáticos e inserindo o loop php wp para retornar os dados cadastrados no dashbord.