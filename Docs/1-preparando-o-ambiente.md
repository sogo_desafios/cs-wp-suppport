# PREPARANDO O AMBIENTE XAMPP/LAMPP
    Para realização do projeto é necessário que tenha instalado no computador, o ambiente XAMPP.

- instalação do Xampp no Windows
    <br> [Tutorial para Windows](https://www.youtube.com/watch?v=rTtb1hkHNBI&ab_channel=AdrianoSouza)

<br>

- instalação do Xampp no Linux (assistir até o minuto: 7:33)
    <br> [Tutorial para Linux](ttps://www.youtube.com/watch?v=Y_GS5OPnd7I&ab_channel=CodeEasy)

<br>

    Após concluída a instalação, podemos excluir todo o conteúdo dentro da pasta [HTDOCS].
    Abrir o aplicativo do XAMPP e clicar em "START ALL" para inicializar os:
        - Mysql Database
        - ProFTPD
        - Apache Web Server
    
    Após todos os serviços inicializados, abra o navegador desejado, e digite: http://localhost/phpmyadmin
        - Clique em "Novo" Banco de Dados
        - Digite o nome: "db_challenge_sogo"
    
    Após criar o banco de dados, insira dentro da pasta htdocs do xampp (ou lampp no ubuntu/linux) o arquivo duplicator-wp-challenge-support.zip presente neste repositório no diretório "/installer".
    Posteriormente extraia os arquivos e deixe somente os arquivos extraídos dentro da pasta HTDOCS.
    São eles:
        - installer.php
        - challenge_sogo_cs_dev_support_037febdb61f608541720_20220209181823_archive.zip
        Lembrando que esses arquivos não podem ser renomeados, e devem obrigatoriamente estar dentro da pasta htdocs.
    
    Após copiados os arquivos, abra o navegador e digite "http://localhost/installer.php" para inicializar o processo de instalação do wordpress via duplicator.

<br>

# Processo de Instalação do Wordpress Duplicator

## Passo 1
    Marque a opção [] I have read and accept all terms & notices.
    Clique em Next

## Passo 2
    Abrirá uma tela de configuração do Duplicator Wordpress solicitando os seguintes dados:
        - Action: Connect and Remove All Data
        - host: localhost
        - Database: db_challenge_sogo
        - User: root
        - Password: (pode deixar em branco)

    Após preencher, clique em NEXT para ir ao passo 3 da configuração.

## Passo 3
    Não alterar nenhuma informação dos campos. Apenas clicar em NEXT para ir ao passo 4 da configuração.

## Passo 4
    Clicar no botão "Admin Login" para realizar autenticação no wordpress.
        - Usuário: desenvolvimento
        - Senha: desafiosogo

<br>

# Problemas de Configuração
    Caso haja algum problema no processo de preparação do ambiente, entre em contato com nosso time via discord.

- [Clique Acessar Discord](https://discord.gg/7r5pbb2XHH)