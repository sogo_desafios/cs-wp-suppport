# Challenge Support Wordpress

    ‼️ O desafio proposto é um site fictício, porém é propriedade intelectual da SOGO Tecnologia e sua equipe. Dessa forma, todo o material disponibilizado aqui (documentação, protótipos, recursos e afins), **não podem ser replicados e divulgados de nenhuma forma e em nenhum meio**, sendo de uso restrito para os interesses da SOGO. 
    Ao participar desta fase do processo seletivo, todos os participantes concordam em manter o sigilo sobre as informações cedidas para realização do desafio proposto.

<br>

# Proposta de desafio

    Este desafio tem como objetivo avaliar o nível de conhecimento técnico sobre as tecnologias utilizadas no desenvolvimento e manutenção de portais e aplicações utilizando o CMS WordPress. Este consiste na realização da manutenção em um site fictício com campos personalizados, através de um plugin, para cadastro de informações. 

    Será disponibilizado para o participante do desafio um microTheme Wordpress que representa um site institucional fictício. O website possui em seu conteúdo principal seções de apresentações das empresas da holding alfa group, sala de imprensa, social media, endereços das sedes, telefones para contato, e mais...

    O desafio será realizar manutenção no website de forma que o mesmo esteja em fidelidade ao layout desenvolvido pelo time de UI/UX disponibilizado no Figma. As atividades a realizar estarão descritas mais abaixo na sua respectiva sessão.

- Figma Project: https://www.figma.com/file/iOnjHqD1JgBY5mC4Tsi3LE/Alfa-Group-Site-Test?node-id=349%3A555

<br>

# Stacks aplicáveis
    Para a realização do desafio é necessário o conhecimento das seguintes tecnologias:

- HTML 5
- CSS 3
- NOÇÕES DE PHP
- WORDPRESS
- ADVANCED CUSTOM FIELDS PRÓ (Plugin Wordpress)
- DUPLICATOR (Plugin Wordpress)

<br>

# Direcionamentos

## Prepando o ambiente de desenvolvimento

Para a realização do desafio é necessário preparar o ambiente de desenvolvimento com algumas tecnologias.

- XAMPP
    <br>Gerenciador Apache PHP, Database MySQL.

- VSCode/Sublime
    <br>IDE de desenvolvimento.

- Discord
    <br>Software de comunicação entre Participante e Líder Técnico.


Todas as informações necessárias para a preparação do ambiente estão descritas nesse próprio repositório, dentro do diretório "/Docs" no arquivo "1 - Preparando o ambiente.md".

<br>

## Descrição do desafio

Todas as informações necessárias para o desenvolvimento das atividades do desafio estão descritas nesse próprio repositório, dentro do diretório "/Docs" no arquivo "2 - Descrição do desafio.md".

<br>

# Dúvidas

    Para tirar dúvidas, os participantes deverão deixar comentários com os seus questionamentos no canal do Discord, que o mais breve possível nossa equipe responderá, ressaltando que, as dúvidas deixadas aqui deverão ser sobre o desafio, dúvidas sobre o processo seletivo devem ser tiradas diretamente com a Coordenadora de Gente e Gestão, Debora, que está conduzindo o processo.

- [Clique Acessar Discord](https://discord.gg/7r5pbb2XHH)

<br>

# ENTREGA

### **PRAZO**
    A resolução do desafio deverá ser enviada em até 3 dias após o recebimento pelo candidato.

### **FORMA DE ENTREGA**
    Ao concluir o desenvolvimento do desafio, deverá ser utilizado o plugin Duplicator, acessível pelo Dashboard do projeto no menu Duplicator, para exportar o projeto. O projeto será exportado com dois arquivos:
    - installer.php
    - file.zip

    obs> Não renomear os arquivos gerados de forma automática pelo plugin duplicator.

    Os arquivos gerados deverão ser enviados por email. 
    Você deverá enviar o desafio para avaliação através do e-mail: vemseralfa@alfagroup.tech
    O título do email deverá ser: [ENTREGA - Desafio Support DEV WP 2022.1] 